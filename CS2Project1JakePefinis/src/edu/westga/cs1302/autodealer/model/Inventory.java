package edu.westga.cs1302.autodealer.model;

import java.util.ArrayList;

import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class Inventory.
 * 
 * @author CS1302
 */
public class Inventory {
	
	/** The Constant START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR. */
	private static final String START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR = "startYear must be <= endYear.";
	
	/** The Constant AUTO_CANNOT_BE_NULL. */
	private static final String AUTO_CANNOT_BE_NULL = "auto cannot be null.";

	/** The autos. */
	private ArrayList<Automobile> autos;

	/**
	 * Instantiates a new inventory.
	 * 
	 * @precondition none
	 * @postcondition size() == 0
	 */
	public Inventory() {
		this.autos = new ArrayList<Automobile>();
	}

	/**
	 * Numbers of autos in inventory.
	 *
	 * @return the number of autos in the inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public int size() {
		return this.autos.size();
	}

	/**
	 * Adds the auto to the inventory.
	 * If the auto's make, model, and year matches an existing auto
	 * it will not be added.
	 *
	 * @param auto the auto
	 * @return true, if add successful
	 * 		   false, if not added
	 * @precondition auto != null
	 * @postcondition size() == size()@prev + 1
	 */
	public boolean add(Automobile auto) {
		if (auto == null) {
			throw new IllegalArgumentException(AUTO_CANNOT_BE_NULL);
		}
		boolean result = true;
		
		
		for (int i = 0; i < this.autos.size(); i++) {
			if (this.autos.get(i).getMake().equals(auto.getMake()) && this.autos.get(i).getModel().equals(auto.getModel()) && this.autos.get(i).getYear() == auto.getYear()) {
				result = false;
			}
		}
		if (!result) {
			return result;
		} else {
			return this.autos.add(auto);
		}
	}

	/**
	 * Adds all the autos to the inventory.
	 *
	 * @param autos the autos to add to the inventory
	 * @return true, if add successful
	 * @precondition autos != null
	 * @postcondition size() == size()@prev + autos.size()
	 */
	public boolean addAll(ArrayList<Automobile> autos) {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		return this.autos.addAll(autos);
	}

	/**
	 * Creates an array that holds the count of the number of automobiles in each
	 * price segment starting from 0 to priceSementRange up to the last range which
	 * includes the maximum price car.
	 *
	 * @param priceSegmentRange the price range
	 * @return Array with number of autos in inventory that are in each price
	 *         segment. Returns null if no autos in inventory.
	 * @precondition none
	 * @postcondition none.
	 */
	public int[] countVehiclesInEachPriceSegment(double priceSegmentRange) {

		if (this.size() == 0) {
			return null;
		}

		double maxPrice = this.findMostExpensiveAuto().getPrice();
		int segments = (int) Math.ceil(maxPrice / priceSegmentRange);
		int[] vehicleCount = new int[segments];

		for (Automobile currAuto : this.autos) {
			double currPrice = currAuto.getPrice() - 0.0001;
			int segmentNumber = (int) (currPrice / priceSegmentRange);
			vehicleCount[segmentNumber]++;
		}

		return vehicleCount;
	}

	/**
	 * Compute average miles of automobiles in inventory.
	 *
	 * @return average miles; if size() == 0 returns 0
	 * @precondition none
	 * @postcondition none
	 */
	public double computeAverageMiles() {
		double sum = 0;

		if (this.autos.size() == 0) {
			return 0;
		}

		for (Automobile currAuto : this.autos) {
			sum += currAuto.getMiles();
		}

		double average = sum / this.autos.size();
		return average;
	}

	/**
	 * Find year of oldest auto.
	 *
	 * @return the year of the oldest auto or Integer.MAX_VALUE if no autos
	 * @precondition none
	 * @postcondition none
	 */
	public int findYearOfOldestAuto() {
		int oldest = Integer.MAX_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() < oldest) {
				oldest = automobile.getYear();
			}
		}

		return oldest;
	}

	/**
	 * Find year of newest auto.
	 *
	 * @return the year of the newest auto or Integer.MIN_VALUE if no autos
	 * @precondition none
	 * @postcondition none
	 */
	public int findYearOfNewestAuto() {
		int newest = Integer.MIN_VALUE;

		for (Automobile automobile : this.autos) {
			if (automobile.getYear() > newest) {
				newest = automobile.getYear();
			}
		}

		return newest;
	}

	/**
	 * Find most expensive auto in the inventory.
	 *
	 * @return the most expensive automobile or null if no autos in inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public Automobile findMostExpensiveAuto() {
		Automobile maxAuto = null;
		double maxPrice = 0;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() > maxPrice) {
				maxAuto = currAuto;
				maxPrice = maxAuto.getPrice();
			}
		}

		return maxAuto;
	}

	/**
	 * Find least expensive auto in the inventory.
	 *
	 * @return the least expensive automobile or null if no autos in inventory.
	 * @precondition none
	 * @postcondition none
	 */
	public Automobile findLeastExpensiveAuto() {
		Automobile minAuto = null;
		double minPrice = Double.MAX_VALUE;

		for (Automobile currAuto : this.autos) {
			if (currAuto.getPrice() < minPrice) {
				minAuto = currAuto;
				minPrice = minAuto.getPrice();
			}
		}

		return minAuto;
	}

	/**
	 * Count autos between specified start and end year inclusive.
	 *
	 * @param startYear the start year
	 * @param endYear   the end year
	 * @return the number of autos between start and end year inclusive.
	 * @precondition startYear <= endYear
	 * @postcondition none
	 */
	public int countAutosBetween(int startYear, int endYear) {
		if (startYear > endYear) {
			throw new IllegalArgumentException(START_YEAR_MUST_BE_LESSTHAN_OR_EQUAL_TO_END_YEAR);
		}
		int count = 0;
		for (Automobile automobile : this.autos) {
			if (automobile.getYear() >= startYear && automobile.getYear() <= endYear) {
				count++;
			}
		}

		return count;
	}

	/**
	 * Deletes the specified auto from the inventory.
	 *
	 * @param auto The automobile to delete from the inventory.
	 * @return true if the auto was found and deleted from the inventory, false
	 *         otherwise
	 * @precondition none
	 * @postcondition if found, size() == size()@prev � 1
	 */
	public boolean remove(Automobile auto) {
		return this.autos.remove(auto);
	}

	/**
	 * Gets the autos.
	 *
	 * @return the autos
	 * @precondition none
	 * @postcondition none
	 */
	public ArrayList<Automobile> getAutos() {
		return this.autos;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "#autos:" + this.size();
	}
	
	
	
	/**
	 * Adds all autos of a specified make in this.autos to an ArrayList
	 *
	 *precondition none
	 *postcondition none
	 *
	 * @param make The make to be searched for.
	 * 
	 * @return an ArrayList of automobiles
	 */
	public ArrayList<Automobile> findAutosOfMake(String make) {
		ArrayList<Automobile> autoList = new ArrayList<Automobile>();
		
		for (Automobile current : this.autos) {
			if (current.getMake().toLowerCase().equals(make.toLowerCase())) {
				autoList.add(current);
			}
		}
		return autoList;
	}

}
