package edu.westga.cs1302.autodealer.model;

/**
 * The Class Dealership.
 * 
 * @author CS1302
 */
public class Dealership {
	private static final String DEALERSHIP_NAME_CANNOT_BE_EMPTY = "name cannot be empty.";
	private static final String DEALERSHIP_NAME_CANNOT_BE_NULL = "name cannot be null.";
	
	private String name;
	private Inventory inventory;

	/**
	 * Instantiates a new dealership.
	 * 
	 * @precondition none
	 * @postcondition getName() == "Unknown"; getInventory().size() == 0
	 */
	public Dealership() {
		this.name = "Unknown";
		this.inventory = new Inventory();
	}	
	
	/**
	 * Instantiates a new dealership.
	 * 
	 * @precondition name cannot be null or empty
	 * @postcondition getName() == name; getInventory().size() == 0
	 *
	 * @param name the name
	 */
	public Dealership(String name) {
		if (name == null) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_NULL);
		}

		if (name.isEmpty()) {
			throw new IllegalArgumentException(DEALERSHIP_NAME_CANNOT_BE_EMPTY);
		}
		this.name = name;
		this.inventory = new Inventory();
	}

	/**
	 * Gets the name.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}
	
	/**
	 * Gets the inventory.
	 *
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the inventory
	 */
	public Inventory getInventory() {
		return this.inventory;
	}

	@Override
	public String toString() {
		return this.name + "; #Autos: " + this.inventory.size();
	}

}
