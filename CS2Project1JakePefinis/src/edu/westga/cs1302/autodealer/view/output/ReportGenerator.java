package edu.westga.cs1302.autodealer.view.output;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.model.Inventory;

/**
 * The Class ReportGenerator.
 * 
 * @author CS1302
 */
public class ReportGenerator {

	private static final String NO_WITH_END_SPACE = "No ";
	private static final String AT_WITH_SURROUNDING_SPACES = " at ";
	private NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance(Locale.US);

	/**
	 * Builds the full summary report of the specified inventory. If inventory is
	 * null, instead of throwing an exception will return a string saying "No
	 * inventory exists.", otherwise builds a summary report of the inventory.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param dealer The dealership to build summary report for
	 *
	 * @return A formatted summary string of the dealership's automobile inventory.
	 */
	public String buildFullSummaryReport(Dealership dealer) {
		String summary = "";

		Inventory inventory = dealer.getInventory();

		if (inventory == null) {
			summary = "No inventory exists.";
		} else {
			summary = dealer.getName() + System.lineSeparator();
			summary += "#Automobiles: " + inventory.size() + System.lineSeparator();
		}

		if (inventory.size() > 0) {
			summary += System.lineSeparator();
			summary += this.buildMostAndLeastExpensiveSummaryOutput(inventory);

			summary += System.lineSeparator();
			summary += this.buildAverageMilesReport(inventory);

			summary += System.lineSeparator();
			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(3000, inventory);

			summary += System.lineSeparator();
			summary += this.buildPriceSegmentBreakdownSummary(10000, inventory);
		}

		return summary;
	}

	private String buildAverageMilesReport(Inventory inventory) {
		String report = "Average miles: ";
		double averageMiles = inventory.computeAverageMiles();

		DecimalFormat milesFormat = new DecimalFormat("#,###.000");
		report += milesFormat.format(averageMiles);

		return report;
	}

	private String buildMostAndLeastExpensiveSummaryOutput(Inventory inventory) {
		Automobile mostExpensiveAuto = inventory.findMostExpensiveAuto();
		Automobile leastExpensiveAuto = inventory.findLeastExpensiveAuto();

		String report = "Most expensive auto: ";
		report += this.buildIndividualAutomobileReport(mostExpensiveAuto) + System.lineSeparator();

		report += "Least expensive auto: ";
		report += this.buildIndividualAutomobileReport(leastExpensiveAuto) + System.lineSeparator();

		return report;
	}

	private String buildPriceSegmentBreakdownSummary(double priceSegmentRange, Inventory inventory) {
		int[] autoPriceSegmentsCount = inventory.countVehiclesInEachPriceSegment(priceSegmentRange);

		if (autoPriceSegmentsCount == null) {
			return "";
		}

		String priceSegmentSummary = "Vehicles in " + this.currencyFormatter.format(priceSegmentRange) + " segments"
				+ System.lineSeparator();

		double startingPrice = 0;
		double endingPrice = priceSegmentRange;

		for (int i = 0; i < autoPriceSegmentsCount.length; i++) {
			String startingPriceCurrencyFormat = this.currencyFormatter.format(startingPrice);
			String endingPriceCurrencyFormat = this.currencyFormatter.format(endingPrice);
			String autoCount = "";

			if (autoPriceSegmentsCount[i] == 0) {
				autoCount = "0";
			}
			for (int index = 0; index < autoPriceSegmentsCount[i]; index++) {
				autoCount += "*";
			}
			priceSegmentSummary += startingPriceCurrencyFormat + " - " + endingPriceCurrencyFormat + " : " + autoCount
					+ System.lineSeparator();

			startingPrice = endingPrice + 0.01;
			endingPrice = (priceSegmentRange * (i + 2));
		}

		return priceSegmentSummary;
	}

	private String buildIndividualAutomobileReport(Automobile auto) {
		String output = this.currencyFormatter.format(auto.getPrice()) + " " + auto.getYear() + " " + auto.getMake()
				+ " " + auto.getModel();
		return output;
	}

	/**
	 * Creates a summary displaying all autos of a specified make within a specified
	 * dealership, also displays the average price of those autos.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param make       the make to be searched for
	 * @param dealership the dealership to be searched through
	 * 
	 * @return String a list of all autos of make in the dealership and their
	 *         average price.
	 */
	public String buildDealershipMakeList(String make, Dealership dealership) {

		Inventory inventory = dealership.getInventory();
		ArrayList<Automobile> array = inventory.getAutos();
		String summary = "Autos by " + make + AT_WITH_SURROUNDING_SPACES + dealership.getName() + ":";
		int makeCount = 0;
		double totalValue = 0;

		for (int i = 0; i < inventory.size(); i++) {
			Automobile current = array.get(i);
			String currentMake = current.getMake();

			if (currentMake.equals(make)) {
				makeCount++;
				totalValue += current.getPrice();
				summary += "\n" + this.buildIndividualAutomobileReport(current);

			}
		}
		summary += "\nAverage Price: " + this.currencyFormatter.format((totalValue / makeCount));
		
		if (makeCount == 0) {
			summary = NO_WITH_END_SPACE + make + AT_WITH_SURROUNDING_SPACES + dealership.getName() + ".";
		}
		return summary + System.lineSeparator();
	}

	/**
	 * Builds a summary of All Autos by dealership.
	 *
	 * @param make the make
	 * @param group the group
	 * @return the string
	 */
	public String buildSummaryForAllAutosByDealership(String make, DealershipGroup group) {

		String summary = "";

		for (Dealership current : group.getDealers()) {
			if (!current.getName().equals("ALL AUTOS")) {
				String dealerSummary = this.buildDealershipMakeList(make, current);
				
				summary += dealerSummary + System.lineSeparator();
			}
		}
		if (summary.equals("")) {
			summary = NO_WITH_END_SPACE + make + "found at any of the dealerships.";
		}
		return summary;
	}

}
