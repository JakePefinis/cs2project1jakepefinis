package edu.westga.cs1302.autodealer.datatier;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Dealership;
import edu.westga.cs1302.autodealer.model.DealershipGroup;
import edu.westga.cs1302.autodealer.resources.UI;

/**
 * The Class AlbumFileWriter.
 * 
 * @author CS1302
 */
public class AutoDealerFileWriter {

	private File inventoryFile;

	/**
	 * Instantiates a new auto file writer.
	 *
	 * @precondition inventoryFile != null
	 * @postcondition none
	 * 
	 * @param inventoryFile the inventory file
	 */
	public AutoDealerFileWriter(File inventoryFile) {
		if (inventoryFile == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.INVENTORY_FILE_CANNOT_BE_NULL);
		}

		this.inventoryFile = inventoryFile;
	}

	/**
	 * Writes all the autos in the dealership to the specified auto file. Each auto
	 * will be on a separate line and of the following format:
	 * make,model,year,miles,price
	 * 
	 * @precondition autos != null
	 * @postcondition none
	 * 
	 * @param autos The collection of autos to write to file.
	 * @throws FileNotFoundException 
	 */
	public void write(ArrayList<Automobile> autos) throws FileNotFoundException {
		if (autos == null) {
			throw new IllegalArgumentException(UI.ExceptionMessages.AUTOS_CANNOT_BE_NULL);
		}

		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Automobile currAuto : autos) {
				String output = currAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
				output += currAuto.getPrice();
				
				writer.println(output);
			}
		}

	}
	
	/**
	 * writes an string line representing an Automobile, includes the dealership.
	 *
	 * @param group the group
	 * @throws FileNotFoundException the file not found exception
	 * @precondition none
	 * @postcondition none
	 */
	public void writeDealershipName(DealershipGroup group) throws FileNotFoundException {
		
		try (PrintWriter writer = new PrintWriter(this.inventoryFile)) {
			for (Dealership currentDealer : group.getDealers()) {
				for (Automobile currentAuto : currentDealer.getInventory().getAutos()) {
					if (!currentDealer.getName().equals("ALL AUTOS")) {
						String output = currentDealer.getName() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currentAuto.getMake() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currentAuto.getModel() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currentAuto.getYear() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currentAuto.getMiles() + AutoDealerFileReader.FIELD_SEPARATOR;
						output += currentAuto.getPrice();
						
						writer.println(output);
					}
				}
			}
		}
	}

}
