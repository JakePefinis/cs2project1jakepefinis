package edu.westga.cs1302.autodealer.test.inventory;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import edu.westga.cs1302.autodealer.model.Automobile;
import edu.westga.cs1302.autodealer.model.Inventory;

class TestFindAutosOfMake {

	@Test
	void testWithEmptyInventory() {
		Inventory inventory = new Inventory();
		
		assertEquals(0, inventory.findAutosOfMake("Ford").size());
	}
	
	@Test
	void testWithOneMatch() {
		Inventory inventory = new Inventory();
		Automobile auto1 = new Automobile("Ford", "Focus", 2018, 300, 1);
		
		inventory.add(auto1);
		
		assertEquals(1, inventory.findAutosOfMake("Ford").size());
	}
	
	@Test
	void testWithMultipleMatches() {
		Inventory inventory = new Inventory();
		Automobile auto1 = new Automobile("Ford", "Focus", 2016, 300, 1);
		Automobile auto2 = new Automobile("Ford", "Focus", 2017, 300, 1);
		Automobile auto3 = new Automobile("Ford", "Focus", 2018, 300, 1);
		
		inventory.add(auto1);
		inventory.add(auto2);
		inventory.add(auto3);
		
		assertEquals(3, inventory.findAutosOfMake("Ford").size());
	}
	
	@Test
	void testWithMultipleMatchesAndSomeNonMatches() {
		Inventory inventory = new Inventory();
		Automobile auto1 = new Automobile("Ford", "Focus", 2016, 300, 1);
		Automobile auto2 = new Automobile("Toyota", "Camry", 2017, 300, 1);
		Automobile auto3 = new Automobile("Ford", "Focus", 2018, 300, 1);
		
		inventory.add(auto1);
		inventory.add(auto2);
		inventory.add(auto3);
		
		assertEquals(2, inventory.findAutosOfMake("Ford").size());
	}
	
	@Test
	void testNoMatchesWithNonEmptyInventory() {
		Inventory inventory = new Inventory();
		Automobile auto1 = new Automobile("Ford", "Focus", 2016, 300, 1);
		Automobile auto2 = new Automobile("Toyota", "Camry", 2017, 300, 1);
		Automobile auto3 = new Automobile("Ford", "Focus", 2018, 300, 1);
		
		inventory.add(auto1);
		inventory.add(auto2);
		inventory.add(auto3);
		
		assertEquals(0, inventory.findAutosOfMake("Nissan").size());
	}
}
